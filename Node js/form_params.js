var http = require('http');
var fs = require('fs');
var parser = require('./params_parser.js');

var p = parser.parse;
var nombre1 = parser.nombre1;

http.createServer(function(req, res){

	if(req.url.indexOf('/favicon.ico') > -1)	
		return;

	fs.readFile('./index.html', function(err, html){

	var html_string = html.toString();
	var variables = html_string.match(/[^\{\}]+(?=\})/g);	

	var varsArray = p(req);
	console.log(varsArray);
	var nombre2 = varsArray.nombre;



	for (var i = variables.length - 1; i >= 0; i--) {
		var value = eval(variables[i]);

		html_string = html_string.replace('{' + variables[i] + '}', value);
	};

	res.writeHead(200,{'Content-Type' : 'text/html'});		

	res.write(html_string);
	res.end();

	});
}).listen(8080);