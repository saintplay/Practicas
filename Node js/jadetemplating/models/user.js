"use strict"

var mongoose = require("mongoose")

mongoose.connect("mongodb://localhost/fotos")

var posibles_valores = ["M","F"]

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,"Colocal un email válido"]

var password_validation = {
	validator: (p)=>{
		return this.__pass_conf == p
	},
	message: "Las contraseñas no son iguales"
}

var user_schema = new mongoose.Schema({
	name: String,
	last_name: String,
	username: {
		type: String,
		required: true,
		maxlength: [10,"Nombre muy largo!!!"]
	},
	password: {
		type: String,
		minlength: [5, "Contraseña muy corta"],
		validate: password_validation
	},
	age: {
		type: Number,
		min: [5,"Tienes que tener por lo menos 5 años"],
		max: [100, "Ya estas muy vieja"]
	},
	sex: {
		type:String,
		enum: {
			values: posibles_valores,
			message: "Opcion no valida"
		}
	},
	email: {
		type: String,
		required: "El correo es obligatorio",
		match: email_match
	},
	birth: Date
})

user_schema.virtual("password_confirmation").get(()=>{
	return this.__pass_conf
}).set((password)=>{
	this.__pass_conf = password
})

user_schema.virtual("full_name").get(()=>{
	return this.name + this.last_name
}).set((full_name)=>{
	var words = full_name.split("")
	this.name = words[0]
	this.last_name = words[1]
})

var User = mongoose.model("User",user_schema)

module.exports.User = User
