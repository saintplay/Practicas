var express = require("express")
var bodyParser = require("body-parser")
var User = require("./models/user").User
var app = express()
var session = require("express-session")
var router_app = require("./models/routes_app")
var session_middleware = require("./middlewares/session.js")

app.use("/public",express.static("public"))
app.use("/public",express.static("assets"))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(session({
	secret: "clavesecretaquenuncasveralaluz",
	resave: false,
	saveUninitialized: false
}))

app.set("views", process.cwd() +  "/templates")
app.set("view engine", "jade")

app.get("/", function(req,res) {	
	console.log(req.session.user_id)
	res.render("index")
})

app.get("/signup", function(req,res) {
	res.render("signup")
})

app.post("/users", function(req,res) {	
		var user = new User({
		username: req.body.username,
		email: req.body.email,
		password: req.body.password,
		password_confirmation: req.body.password_confirmation
	})
	user.save().then((us)=>{
		res.send("Guardamos el usuario exitosamente")
	}, (err)=>{
		if(err) {
			console.log(String(err))
			res.send("No pudimos guardar el usuario")
		}
	})
})

app.get("/login", function(req,res) {	
	res.render("login")
})

app.post("/sessions",(req,res)=>{

	User.findOne({email: req.body.email, password: req.body.password},(err,user)=>{
		req.session.user_id = user._id
		res.send("Hola Mundo")
	})
})

app.use("/app",session_middleware)
app.use("/app",router_app)

app.listen(process.argv[2] || 8080)
