function parse(req){

	var arreglo_parametros = [];
	var parametros = {};
	var questionIndex = req.url.indexOf('?');
	
	if(questionIndex > -1)
	{
		var url_data = req.url.slice(questionIndex + 1);
		arreglo_parametros = url_data.split("&");
	}

	for (var i = arreglo_parametros.length - 1; i >= 0; i--) {
		var parametro = arreglo_parametros[i];
		var parametro_data = parametro.split('=');
		parametros[parametro_data[0]] = parametro_data[1];
	};

	return parametros;
}


module.exports.parse = parse;
module.exports.nombre1 = 'Diego';