module.exports = (grunt) => {
	grunt.initConfig({
		slim: {
			dist: {
				files: {
					"public/index.html":"src/views/index.slim"
				}
			}
		},
		less: {
			"public/css/styles.css" : "src/stylesheet/styles.less"
		}
	})

	grunt.loadNpmTasks("grunt-slim")
	grunt.loadNpmTasks("grunt-contrib-less")

	grunt.registerTask("default",["less"])
}
