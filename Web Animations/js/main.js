var main = function(){
	var btnFormOpen = document.getElementById('form-open');
	var btnFormClose = document.getElementById('form-close');
	var body = document.getElementById('to-body');
	var modal = document.getElementById('modal');

	btnFormOpen.addEventListener('click', function(){
		body.classList.add('active');
		modal.classList.add('active');
	});

	btnFormClose.addEventListener('click', function(){
		body.classList.remove('active');
		modal.classList.remove('active');
	});

}

document.addEventListener('DOMContentLoaded',main);