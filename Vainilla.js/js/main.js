var item3, item;

document.addEventListener('DOMContentLoaded', function(){
	var $ = function(element){
		return document.querySelector(element);
	};

	var $$ = function(element){
		return document.querySelectorAll(element);
	};

	var select = function(element){
		if(element[0] === '#')
			return document.querySelector(element);
		else
			return document.querySelectorAll(element);
	};

	var toggleMenu = document.getElementById('toggle-menu');
	var menu = document.getElementById('menu');

	toggleMenu.addEventListener('click', function() {
		menu.classList.toggle('mostrar');
	});

	document.body.style.background = 'yellow';
	document.getElementById('solicitudin').style.color = 'blue';
	document.getElementsByClassName('lorem')[0].style.color = 'red';
	document.getElementsByTagName('h1')[0].style.color = 'purple';

	item3 = select('#item3');
	items = select('.menu_item');

	item3.style.background = 'red';
	item3.nextElementSibling.style.background = 'blue';
	item3.previousElementSibling.style.background = 'lightblue';
	item3.parentNode.style.border = '3px solid black';
	item3.children[0].style.color = 'white';
	select('ul')[0].children[4].children[0].style.color = 'red';


});