var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var	runMode = require('gulp-sync')(gulp);

gulp.task('refresh', function(){
	browserSync.reload();
});

gulp.task('go-sass',function() {
	return gulp.src('css/*.sass')
		.pipe(sass({ outputStyle: 'expanded' }))
		.pipe(gulp.dest('css'));
});

gulp.task('go-min-sass', function() {
	return gulp.src('css/*.sass')
		.pipe(sass({ outputStyle: 'compressed' }))
		.pipe(gulp.dest('css'));
});

gulp.task('go-jade', function(){
	return gulp.src('views/*.jade')
		.pipe(jade({ pretty: true }))
		.pipe(gulp.dest('views'));
});

gulp.task('go-min-jade', function(){
	return gulp.src('views/*.jade')
		.pipe(jade({
			pretty: false
		}))
		.pipe(gulp.dest('views'));
});

gulp.task('go-sass-jade', ['go-sass','go-jade']);

gulp.task('go-min-sass-jade', ['go-min-sass','go-min-jade']);

gulp.task('sync-sass',function() {
	browserSync.init({
        server: './',
        index: 'views/index.html'
    });

	gulp.watch('css/*.sass', runMode.sync(['go-sass','refresh']));
});

gulp.task('sync-jade', function() {
	browserSync.init({
		server: './',
		index: 'views/index.html'
	});

	gulp.watch('views/*.jade', runMode.sync(['go-jade','refresh']));
});

gulp.task('sync-sass-jade',function(){
	browserSync.init({
		server: './',
		index:'views/index.html',
		notify: false
	});

	gulp.watch('css/*.sass', runMode.sync(['go-sass','refresh']));
	gulp.watch('views/*.jade', runMode.sync(['go-jade','refresh']));
});

gulp.task('default', ['sync-sass-jade']);