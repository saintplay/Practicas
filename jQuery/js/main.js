$(document).ready(function(){

	$("h3").hide();
	//Podemos editar ciertos atributos css
	$("h1").css('background-color', 'steelblue');
	//Podemos editar elementos que estan d
	$('#linksVerdes a').css({
		'color': '#2CDE20',
		'display': 'block'
	});
	$('p > em').css('color', 'gray');
	//Podemos editar un elemento si es precedido por otro
	$('em + a').css('color', 'blue');
	//Podemos editar elemenos si presentan un atributo
	//tmbn se puede buscar atributos con :has(name)
	$('li[name]').html('Este li tiene un atributo name');
	//Para cambiar el valor de un input se usa val
	$("input[type='text']").val('Ingrese un texto');
	//Podemos buscar partes de un atributo con el *
	$("li[name*='penult']").css("font-weight",'bolder');
	//Podemos buscar el inicio de un atributo con el ^
	$("li[name^='segun']").css({
		'border-color':'black',
		'border-width':'4px',
		'border-style':'solid'
	});
	//Podemos buscar el final de un atributo
	$("li[name$='cerli'").css('color','orange');
	//Podemos targetear al primer elemento
	$("li:first").css('color','#49E268');
	//Podemos targetear al ultimo elemento
	$('li:last').css('color','#8D0864');
	//Podemos targetear un elemento segun su posicion
	//Se puede usar tmbn .text pero no sera posible hacer un
	//apend 
	$('#lista li:nth-child(6)').html('Li cambiado');
	//Podemos targetear elemenos pares e impares
	$("li:even").css('background-color','#DA545C');
	$("li:odd").css('background-color','#98F6FF');

	//Podemos targetear elementos segun su contenido
	$("p:contains(nec)").css('font-family','monospace');

	//Podemos usar el not
	$("p:not(:contains(nec))").css('background-color','red');

	//Se puede usar append y prepend
	$("p:contains(nec)").append(' voy al final');
	$("p:contains(nec)").prepend('voy al inicio ')

	//Se puede agrega elementos con before y after
	$("p:contains(nec)").before('<p>soy un parrafo agregado con before </p>');
	$("p:contains(nec)").after('<p>soy un parrafo agregado con after</p>');

	

	var $cuadrado = $('#cuadrado');

	$("h2").on('click',function(e){
		$(this).next().slideToggle(300);
	})

	$('#botones').on('click','a',function(e){
		$cuadrado.removeClass().addClass($(this).attr('id'));
		console.log(e.target);
	})

	$(document).on('keypress', function(e){
		console.log(e.charCode);
	})	
})

