var express = require('express');
var router = express.Router();

var mongodb = require("mongodb");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/thelist', function(req, res){
	var mongoClient = mongodb.MongoClient;
	var url= "mongodb://localhost:27017/mean";

	mongoClient.connect(url, (err,db)=>{
		var collection = db.collection('students');

		collection.find({}).toArray((err, result)=>{
			if (err) {
				res.send(err);
			} else if(result.length) {
				res.render("studentlist",{"studentlist": result});
			}	else {
				res.render("No document found!!!");
			}	
			db.close();
		});
	});
});

module.exports = router;
