var moduleName = "mean"

var mainModule = angular.module(moduleName, [])

angular.element(document).ready(()=>{
	angular.bootstrap(document, [moduleName])
})
