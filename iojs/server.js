'use strict'

const http = require('http');
const puerto = process.env.PORT || 8080;
const server = http.createServer();
server.on('request',onRequest);
server.on('listening',onListening);
server.listen(puerto);
function onRequest(req,res){
	res.end('hola io,js');
}
function onListening(){
	console.log("Servidor escuchando en puerto " + puerto);
}

