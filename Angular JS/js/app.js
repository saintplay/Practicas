(function(){

	var app =  angular.module('myApp',[]);

	app.controller('StoreController', function(){
		this.list = products;
	});

	var products = [
		{
			name : 'Oculust Rift Full Experience',
			price : '$ 1500.00',
			description : 'Virtual reality never was so reality!',
			canPurchase : true,
			soldOut : false,
		},
		{
			name : 'Holo Lens',
			price : '$ 2500.00',
			description : 'Like your children dreams!',
			canPurchase : false,
			soldOut : false,
		}
	]

})();

